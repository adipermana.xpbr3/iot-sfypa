<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container-fluid mb-3">
    <h4 class="h4 mb-0 font-weight-bold text-gray-800">Monitoring Data Sensor</h4>
    <div class="d-sm-flex align-items-center justify-content-end mb-3">
        <div class="text-xl font-weight-bold text-gray-800 mr-1">
            Diperbarui : <span id="created">- -</span>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-secondary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-secondary text-uppercase mb-1">
                                Gas</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" ><span id="gas">0</span></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-skull-crossbones fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                                Red </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><span id="red">0</span></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Green
                            </div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><span id="green">0</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                Blue</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><span id="blue">0</span></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                Tingkat Kesegaran Daging</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><span id="kriteria">- -</span></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-drumstick-bite fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-2 mb-4 justify-content-center">
        <!-- Area Chart -->
        <div class="col-xl-6 col-lg-6">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-info">Data Gas</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area text-center">
                        <iframe width="450" height="260" style="border: 1px solid #cccccc;" src="https://thingspeak.com/channels/1498745/charts/1?bgcolor=%23ffffff&color=%23d62020&dynamic=true&results=25&type=line&update=15"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-lg-6">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-info">Data Red</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area text-center">
                    <iframe width="450" height="260" style="border: 1px solid #cccccc;" src="https://thingspeak.com/channels/1498745/charts/2?bgcolor=%23ffffff&color=%23d62020&dynamic=true&results=25&type=line&update=15"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-lg-6">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-info">Data Green</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area text-center">
                    <iframe width="450" height="260" style="border: 1px solid #cccccc;" src="https://thingspeak.com/channels/1498745/charts/3?bgcolor=%23ffffff&color=%23d62020&dynamic=true&results=25&type=line&update=15"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-lg-6">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-info">Data Blue</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area text-center">
                    <iframe width="450" height="260" style="border: 1px solid #cccccc;" src="https://thingspeak.com/channels/1498745/charts/4?bgcolor=%23ffffff&color=%23d62020&dynamic=true&results=25&type=line&update=15"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>
