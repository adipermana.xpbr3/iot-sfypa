<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container-fluid mb-3">
    <div class="row">
        <div class="col-lg-8">
            <div class="card shadow">
                <div class="card-body">
                    <a href="" class="btn btn-success mb-3" data-toggle="modal" data-target="#addSample"><i class="fas fa-fw fa-plus mr-1"></i>Tambah Data</a>
                    <div class="card">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Table Data Sample</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="table_id" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th scope="col" rowspan="2" class="text-center align-middle">No</th>
                                            <th scope="col" rowspan="2" class="text-center align-middle">Kriteria</th>
                                            <th scope="col" rowspan="2" class="text-center align-middle">Gas</th>
                                            <th scope="col" colspan="3" class="text-center">Warna</th>
                                            <th scope="col" rowspan="2" class="text-center align-middle">Aksi</th>
                                        </tr>
                                        <tr>
                                            <td class="text-center bg-danger text-white">Red</td>
                                            <td class="text-center bg-success text-white">Green</td>
                                            <td class="text-center bg-primary text-white">Blue</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1 ; foreach($sample as $sample) :?>
                                            <tr>
                                                <td class="text-center"><?php echo $no++ ?></td>
                                                <td><?php echo $sample->kriteria ?></td>
                                                <td class="text-center"><?php echo $sample->gas ?></td>
                                                <td class="text-center"><?php echo $sample->red ?></td>
                                                <td class="text-center"><?php echo $sample->green ?></td>
                                                <td class="text-center"><?php echo $sample->blue ?></td>
                                                <td class="text-center">
                                                    <a href="" class="badge badge-success mr-1" data-toggle="modal" data-target="#updateSample<?php echo $sample->id ?>">ubah</a>
                                                    <a href="<?= base_url('delete-sample/'.$sample->id)?>" class="badge badge-danger">hapus</a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>

<div class="modal fade" id="addSample" tabindex="-1" aria-labelledby="addSampleLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addSampleLabel">Tambah Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="<?= base_url('add-sample') ?>">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Data Gas</label>
                        <input name="gas" type="decimal" class="form-control" placeholder="Masukan nilai gas..." autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label>Red</label>
                        <input name="red" type="decimal" class="form-control" placeholder="Masukan nilai red..." autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label>Green</label>
                        <input name="green" type="decimal" class="form-control" placeholder="Masukan nilai green..." autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label>Blue</label>
                        <input name="blue" type="decimal" class="form-control" placeholder="Masukan nilai blue..." autocomplete="off" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
    foreach($update as $u):
       $id          = $u->id;
       $kriteria    = $u->kriteria;
       $gas         = $u->gas;
       $red         = $u->red;
       $green       = $u->green;
       $blue        = $u->blue;
?>
     
<!-- Modal Update Menu -->
<div class="modal fade" id="<?php echo "updateSample".$id;?>" tabindex="-1" aria-labelledby="updateSampleLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="updateSampleLabel">Ubah Sample</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="<?= base_url('update-sample')?>">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Data Gas</label>
                        <input name="id" type="hidden" value="<?= $id ?>" class="form-control" autocomplete="off" required>
                        <input name="gas" type="decimal" value="<?= $gas ?>" class="form-control" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label>Red</label>
                        <input name="red" type="decimal" value="<?= $red ?>" class="form-control" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label>Green</label>
                        <input name="green" type="decimal" value="<?= $green ?>" class="form-control" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label>Blue</label>
                        <input name="blue" type="decimal" value="<?= $blue ?>" class="form-control" autocomplete="off" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php endforeach;?>
