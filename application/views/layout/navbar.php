<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Topbar -->
<nav class="navbar navbar-expand-lg bg-gradient-primary topbar mb-4 static-top shadow navbar-dark">
    <h4 class="h4 ml-2 text-white">Sistem Monitoring Kesegaran Daging Ayam</h4>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-md-auto">
            <li class="nav-item active">
                <a class="nav-link" href="<?= base_url() ?>">Monitoring <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('data-sample') ?>">Data Sampel</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('analisi-data') ?>">Analisis Data</a>
            </li>            
        </ul>
    </div>
</nav>
<!-- End of Topbar -->