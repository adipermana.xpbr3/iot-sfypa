<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function index() {

        $this->load->view('layout/header');
        $this->load->view('layout/navbar');
        $this->load->view('dashboard');
        $this->load->view('layout/footer');
    }
	
	public function rest()
	{
		$url = "https://api.thingspeak.com/channels/1498745/feeds/last";
		$get_url = file_get_contents($url);
		$data = json_decode($get_url, true);	
		if($data > 0) {	
			$gas = $data['field1'];
			$red = $data['field2'];
			$green = $data['field3'];
			$blue = $data['field4'];
			
			if ($red > 73 && $red < 95 && $green > 80 && $green < 100 && $blue > 60 && $blue < 106) {
				$kriteria = 'Segar';
			} else {
				$kriteria = 'Tidak Segar';
			};
			$array = array(
				'success' 	=> true,
				'message' 	=> 'OK',
				'data'		=> array(
					'gas' 		=> $gas,
					'red' 		=> $red,
					'green' 	=> $green,
					'blue' 		=> $blue,
					'kriteria'	=> $kriteria,
					),
			);
		} else {
			$array = array(
				'success' 	=> false,
				'message' 	=> 'not found',
			);
		}
		header('Content-Type: application/json');
		echo json_encode($array, JSON_PRETTY_PRINT);
	}
}