<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataSample extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();

		date_default_timezone_set('Asia/Jakarta');
	}

    public function index() {

        $data['sample'] = $this->db->get('fx_m_sample')->result();
        $data['update'] = $this->db->get('fx_m_sample')->result();
        $this->load->view('layout/header');
        $this->load->view('layout/navbar');
        $this->load->view('dataSample', $data);
        $this->load->view('layout/footer');
    }

    public function addSample() {

        $gas     = $this->input->post('gas');
        $red     = $this->input->post('red');
        $green   = $this->input->post('green');
        $blue    = $this->input->post('blue');
		$created = date('d/m/Y, H:m:i');
		//var_dump($created); die;

        if ($red > 73 && $red < 95 && $green > 80 && $green < 100 && $blue > 60 && $blue < 106) {
            $kriteria = 'Segar';
        } else {
            $kriteria = 'Tidak Segar';
        };

        $data = array (

            'kriteria'  => $kriteria,
            'gas'       => $gas,
            'red'       => $red,
            'green'     => $green,
            'blue'      => $blue,
			'created'   => $created,
        );

        $this->db->insert('fx_m_sample', $data);
        redirect('data-sample');
    }

    public function updateSample() {

        $id      = $this->input->post('id');
        $gas     = $this->input->post('gas');
        $red     = $this->input->post('red');
        $green   = $this->input->post('green');
        $blue    = $this->input->post('blue');

        if ($red > 73 && $red < 95 && $green > 80 && $green < 100 && $blue > 60 && $blue < 106) {
            $kriteria = 'Segar';
        } else {
            $kriteria = 'Tidak Segar';
        };

        $data = array (

            'kriteria'  => $kriteria,
            'gas'       => $gas,
            'red'       => $red,
            'green'     => $green,
            'blue'      => $blue,
        );

        $where = array(
            'id' => $id,
        );

        $this->db->update('fx_m_sample', $data, $where);
        redirect('data-sample');
    }

    public function deleteSample($id) {

        $where = array('id' => $id);
        $this->db->where($where);
        $this->db->delete('fx_m_sample');
        redirect('data-sample');
    }

    public function postSample() {

        $kriteria   = $this->input->post('kriteria');
        $gas        = $this->input->post('gas');
        $red        = $this->input->post('red');
        $green      = $this->input->post('green');
        $blue       = $this->input->post('blue');
        $created    = $this->input->post('created');
        
        $data = array (
            
            'kriteria'  => $kriteria,
            'gas'       => $gas,
            'red'       => $red,
            'green'     => $green,
            'blue'      => $blue,
            'created'   => $created,
        );
        
        $result = $this->db->query("SELECT * FROM fx_m_sample WHERE created = '$created'")->num_rows();

        if ($result <= 0) {
            
            $this->db->insert('fx_m_sample', $data);
			$arr = array(
				'success' => true,
				'message' => 'data berhasil di insert.');
			header('Content-Type: application/json');
			echo json_encode($arr);
            
        } else {
			$arr = array(
				'success' => false,
				'message' => 'data gagal di insert.');
			header('Content-Type: application/json');
			echo json_encode($arr);
		};

    }
}