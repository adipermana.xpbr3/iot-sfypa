/*
 Navicat Premium Data Transfer

 Source Server         : si-bppj
 Source Server Type    : MySQL
 Source Server Version : 80023
 Source Host           : ik1eybdutgxsm0lo.cbetxkdyhwsb.us-east-1.rds.amazonaws.com:3306
 Source Schema         : oyqb9qwnchb2owlk

 Target Server Type    : MySQL
 Target Server Version : 80023
 File Encoding         : 65001

 Date: 12/01/2022 00:26:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for fx_m_sample
-- ----------------------------
DROP TABLE IF EXISTS `fx_m_sample`;
CREATE TABLE `fx_m_sample`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `kriteria` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gas` int(0) NULL DEFAULT NULL,
  `red` int(0) NULL DEFAULT NULL,
  `green` int(0) NULL DEFAULT NULL,
  `blue` int(0) NULL DEFAULT NULL,
  `created` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 103 CHARACTER SET = latin1 COLLATE latin1_swedish_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
