/*
 Navicat Premium Data Transfer

 Source Server         : Mysql
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : localhost:3306
 Source Schema         : db_iot

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 27/01/2022 00:36:58
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for fx_m_sample
-- ----------------------------
DROP TABLE IF EXISTS `fx_m_sample`;
CREATE TABLE `fx_m_sample`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kriteria` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gas` decimal(11, 2) NULL DEFAULT NULL,
  `red` decimal(11, 2) NULL DEFAULT NULL,
  `green` decimal(11, 2) NULL DEFAULT NULL,
  `blue` decimal(11, 2) NULL DEFAULT NULL,
  `created` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;
